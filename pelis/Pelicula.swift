//
//  Pelis.swift
//  pelis
//
//  Created by Antonio Pertusa on 14/12/16.
//  Copyright © 2016 Antonio Pertusa. All rights reserved.
//

import UIKit

class Pelicula
{
    var id: Int?
    var title : String?
    var year : Int?
    var director : String?
    var urlPoster : String?
    var rented : Bool?
    var synopsis : String?
    var image : UIImage?
    
    init?()
    {
        self.title = ""
        self.year = 1900
        self.director  = ""
        self.urlPoster = ""
        self.rented = false
        self.synopsis = ""
    }
    
    init(title: String, year: Int, director: String, urlPoster: String, rented: Bool, synopsis: String)
    {
        self.title = title
        self.year = year
        self.director  = director
        self.urlPoster = urlPoster
        self.rented = rented
        self.synopsis = synopsis
    }
    
    init(_ copy: Pelicula) {
        self.title = copy.title
        self.year = copy.year
        self.director  = copy.director
        self.urlPoster = copy.urlPoster
        self.rented = copy.rented
        self.synopsis = copy.synopsis
        self.id = copy.id
        self.image = copy.image
    }
    
    init?(json: [String:Any]) {
        guard let title = json["title"] as? String,
            let year = json["year"] as? String,
            let id = json["id"] as? String,
            let director = json["director"] as? String,
            let urlPoster = json["poster"] as? String,
            let rented = json["rented"] as? String,
            let synopsis = json["synopsis"] as? String
            else {
                return nil
        }
        self.id = Int.init(id)
        self.title = title
        self.year = Int.init(year)
        self.director = director
        self.urlPoster = urlPoster
        self.rented = rented == "1"
        self.synopsis = synopsis
    }
    
    func toJSON() -> Data? {
        var dict: [String: Any] = [:]
        dict.updateValue(title!, forKey: "title")
        dict.updateValue(year!, forKey: "year")
        dict.updateValue(director!, forKey: "director")
        dict.updateValue(urlPoster!, forKey: "poster")
        dict.updateValue(rented!, forKey: "rented")
        dict.updateValue(synopsis!, forKey: "synopsis")
        
        do {
            let data = try JSONSerialization.data(withJSONObject: dict, options: [])
            return data
        } catch {
            print(error)
        }
        
        return nil
    }
}


