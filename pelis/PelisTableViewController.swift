//
//  PelisTableViewController.swift
//  pelis
//
//  Created by Antonio Pertusa on 19/12/16.
//  Copyright © 2016 Antonio Pertusa. All rights reserved.
//

import UIKit

protocol PelisTableDelegate {
    func saved(peli : Pelicula)
}

class PelisTableViewController: UITableViewController, UITextFieldDelegate, UITextViewDelegate, ConnectionDelegate {
    
    var peli = Pelicula()!
    var delegate:PelisTableDelegate?
 
    override func viewDidLoad() {
        super.viewDidLoad()

        // This will remove extra separators from tableview
        self.tableView.tableFooterView = UIView(frame: .zero)
        
        // Save button
        let saveButton = UIBarButtonItem(title: "Save", style: .done, target: self, action: #selector(save))
        self.navigationItem.rightBarButtonItem = saveButton
        
        if (self.peli.title?.isEmpty)! {
            self.navigationItem.title = "Add movie"
        } else {
            self.navigationItem.title = self.peli.title
        }
    }
    
    func getCellTextField(section : Int) -> String?
    {
        let cell = tableView.cellForRow(at: IndexPath(row: 0, section: section))
        let textField = cell?.subviews[1] as? UITextField
        return textField?.text
    }
    
    func getCellTextView(section : Int) -> String?
    {
        let cell = tableView.cellForRow(at: IndexPath(row: 0, section: section))
        let textView = cell?.subviews[1] as? UITextView
        return textView?.text
    }
    
    func save() {
        
        if let title = self.getCellTextField(section: 0) {
            self.peli.title = title
        }
        if let year = self.getCellTextField(section: 1) {
            self.peli.year = Int(year)
        }
        if let director = self.getCellTextField(section: 2) {
            self.peli.director = director
        }
        if let poster = self.getCellTextField(section: 3) {
            self.peli.urlPoster = poster
        }
        if let synopsis = self.getCellTextView(section: 4) {
            self.peli.synopsis = synopsis
        }
        
        // Crear JSON y enviar al servidor petición para guardar la peli.
        // Ojo, cuando se recibe la respuesta hay que guardar el id (en connectionSucceed)
        if let filmJSON = self.peli.toJSON() {
            if self.peli.id != nil {
                
                updateFilmInServer(with: filmJSON)
            } else {
                saveFilmInServer(with: filmJSON)
            }
        }
    }
    
    private func updateFilmInServer(with json: Data) {
        let session = URLSession(configuration: .default)
        let url = "http://gbrain.dlsi.ua.es/videoclub/api/v1/catalog/\(self.peli.id!)"
        let credentials = ("test123", "test123")
        let connection = Connection(name: "update", delegate: self)
        
        var request = URLRequest(url: URL(string: url)!)
        request.httpBody = json
        request.httpMethod = "PUT"
        request.setValue(connection.getCredentials(credentials), forHTTPHeaderField: "Authorization")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        connection.startConnection(session, with: request)
    }
    
    private func saveFilmInServer(with json: Data) {
        let session = URLSession(configuration: .default)
        let url = "http://gbrain.dlsi.ua.es/videoclub/api/v1/catalog"
        let credentials = ("test123", "test123")
        let connection = Connection(name: "create", delegate: self)
        
        var request = URLRequest(url: URL(string: url)!)
        request.httpBody = json
        request.httpMethod = "POST"
        request.setValue(connection.getCredentials(credentials), forHTTPHeaderField: "Authorization")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        connection.startConnection(session, with: request)
    }
    
    func connectionSucceed(_ connection: Connection, with data: Data) {
        switch connection.name! {
        case "create":
            if let jsonData = try? JSONSerialization.jsonObject(with: data, options: []) as! [String:Any] ,
                let id = jsonData["id"] as? Int {
                
                self.peli.id = id
                
                self.delegate?.saved(peli: self.peli)
                _ = self.navigationController?.popViewController(animated: true)
            }
        case "update":
            self.delegate?.saved(peli: self.peli)
            _ = self.navigationController?.popViewController(animated: true)
        default:
            break
        }
    }
    
    func connectionFailed(_ connection: Connection, with error: String) {
        print(error)
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 5
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch(section) {
        case 0: return "Title"
        case 1: return "Year"
        case 2: return "Director"
        case 3: return "URL poster"
        case 4: return "Synopsis"
        default: return nil
        }
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = UITableViewCell(style: .subtitle, reuseIdentifier: "Cell")
        cell.accessoryType = .none
        
        if (indexPath.section != 4) {
            let textField = UITextField(frame: CGRect(x:10,y:10,width:self.tableView.frame.size.width-10, height:30))
            
            textField.adjustsFontSizeToFitWidth = true
            textField.delegate = self
            textField.tag = indexPath.section
            textField.isEnabled = true
           
            if indexPath.section==0 {
                textField.text=self.peli.title
            }
            else if indexPath.section == 1 {
                if let year = peli.year {
                    textField.text = "\(year)"
                }
            }
            else if indexPath.section == 2 {
                textField.text=self.peli.director
            }
            else if indexPath.section == 3 {
                textField.text=self.peli.urlPoster
            }
            
            cell.addSubview(textField)
        }
        else {
            let textView = UITextView(frame: CGRect(x:10, y:10, width:self.tableView.frame.size.width-10, height:300))
            textView.font = textView.font?.withSize(15)
            textView.delegate = self
            
            textView.text=self.peli.synopsis

            cell.addSubview(textView)
        }
        
        return cell
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 4 {
            return 300
        }
        return 40
    }
    
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    // MARK: - TextField
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return true
    }
   
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField.tag {
        case 0: self.peli.title = textField.text
        case 1: self.peli.year = Int(textField.text!)
        case 2: self.peli.director = textField.text
        case 3: self.peli.urlPoster = textField.text
        default: print("Unknown!") //  Esto no debería pasar nunca, añadido para silenciar el warning
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        self.peli.synopsis = textView.text
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
}
