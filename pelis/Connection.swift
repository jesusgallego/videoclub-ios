//
//  Connection.swift
//  pelis
//
//  Created by Master Móviles on 17/1/17.
//  Copyright © 2017 Antonio Pertusa. All rights reserved.
//

import Foundation

protocol ConnectionDelegate {
    func connectionSucceed(_ connection : Connection, with data: Data)
    func connectionFailed(_ connection: Connection, with error: String)
}

class Connection {
    
    var delegate:ConnectionDelegate?
    var name: String?
    
    init(name: String, delegate : ConnectionDelegate) {
        self.name = name
        self.delegate = delegate
    }
    
    func startConnection(_ session: URLSession, with request:URLRequest)
    {
        session.dataTask(with: request, completionHandler: { data, response, error in
            
            if let error = error {
                self.delegate?.connectionFailed(self, with: error.localizedDescription)
            }
            else {
                let res = response as! HTTPURLResponse
                
                if res.statusCode == 200 {
                    DispatchQueue.main.async {
                        self.delegate?.connectionSucceed(self, with: data!)
                    }
                }
                else {
                    let errorMessage=("Received status code: \(res.statusCode)")
                    self.delegate?.connectionFailed(self, with: errorMessage)
                }
            }
        }).resume()
    }
    
    func startConnection(_ session: URLSession, with url:URL, method: String, and credentials: (String, String))
    {
            
            let authValue = getCredentials(credentials)
            
            // Request
            var request = URLRequest(url: url)
            request.httpMethod = method
            request.addValue(authValue!, forHTTPHeaderField: "Authorization")
            
            self.startConnection(session, with: request)
    }
    
    func startConnection(_ session: URLSession, with url:URL, method: String = "GET")
    {
        self.startConnection(session, with: URLRequest(url:url))
    }
    
    func startConnection(_ session: URLSession, with urlString:String, method: String = "GET")
    {
        if let encodedString = urlString.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed) {
            if let url = URL(string: encodedString) {
                self.startConnection(session, with: url, method: method)
            }
        }
    }
    
    func startConnection(_ session: URLSession, with urlString: String, method: String = "GET", and credentials: (String, String))
    {
        if let encodedString = urlString.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed) {
            if let url = URL(string: encodedString) {
                self.startConnection(session, with: url, method: method, and: credentials)
            }
        }
    }
    
    func getCredentials(_ credentials: (String,String)) -> String?
    {
        let authStr = "\(credentials.0):\(credentials.1)"
        
        if let authData = authStr.data(using: .utf8) {
            
            return "Basic \(authData.base64EncodedString()))"
        }
        
        return nil
    }
}

