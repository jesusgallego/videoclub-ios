//
//  MasterViewController.swift
//  pelis
//
//  Created by Antonio Pertusa on 14/12/16.
//  Copyright © 2016 Antonio Pertusa. All rights reserved.
//

import UIKit

class MasterViewController: UITableViewController, ConnectionDelegate, PelisTableDelegate {

    var detailViewController: DetailViewController? = nil
    var pelis = [Pelicula]()
    var session: URLSession!
    var deletedIndexPath: IndexPath!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Create Session
        self.session = URLSession(configuration: URLSessionConfiguration.default)
  
        self.createPelis()
        
        
        // Do any additional setup after loading the view, typically from a nib.
        self.navigationItem.leftBarButtonItem = self.editButtonItem

        let addButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(insertNewObject(_:)))
        self.navigationItem.rightBarButtonItem = addButton
        if let split = self.splitViewController {
            let controllers = split.viewControllers
            self.detailViewController = (controllers[controllers.count-1] as! UINavigationController).topViewController as? DetailViewController
        }
 
    }

    override func viewWillAppear(_ animated: Bool) {
        self.clearsSelectionOnViewWillAppear = self.splitViewController!.isCollapsed
        super.viewWillAppear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func insertNewObject(_ sender: Any) {
        let pelisAdd = PelisTableViewController()
        pelisAdd.delegate = self
        self.navigationController?.pushViewController(pelisAdd, animated: true)
    }

    // MARK: - Segues

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetail" {
            if let indexPath = self.tableView.indexPathForSelectedRow {
                let object = pelis[indexPath.row]
                let controller = (segue.destination as! UINavigationController).topViewController as! DetailViewController
                controller.detailItem = object
                controller.navigationItem.leftBarButtonItem = self.splitViewController?.displayModeButtonItem
                controller.navigationItem.leftItemsSupplementBackButton = true
            }
        }
    }

    // MARK: - Table View

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.pelis.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)

        let object = self.pelis[indexPath.row]
        cell.textLabel!.text = object.title
        cell.imageView?.contentMode = .scaleAspectFit
        if let image = object.image {
            cell.imageView?.image = image
        } else {
            cell.imageView?.image = UIImage(named: "Placeholder.png")
        
            if let url = URL(string: object.urlPoster!) {
                session.dataTask(with: url) { data, response, error in
                    if error == nil,
                        let data = data,
                        let res = response as? HTTPURLResponse,
                        res.statusCode == 200 {
                        DispatchQueue.main.async {
                            let img = UIImage(data: data)
                            object.image = img
                            cell.imageView?.image = img
                        }
                    }
                
                }.resume()
            }
        }
        
        return cell
    }

    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }

    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let url = "http://gbrain.dlsi.ua.es/videoclub/api/v1/catalog/\(self.pelis[indexPath.row].id!)"
            let credentials = ("test123", "test123")
            let connection = Connection(name: "delete", delegate: self)
            connection.startConnection(self.session, with: url, method: "DELETE", and: credentials)
            
            self.deletedIndexPath = indexPath
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
        }
    }
  

    // Función para inicializar los valores de las películas
    func createPelis()
    {
        let url = "http://gbrain.dlsi.ua.es/videoclub/api/v1/catalog"
        
        let connection = Connection(name: "get", delegate: self)
        connection.startConnection(self.session, with: url)
    }
    
    // MARK: - Conenction Delegate
    
    func connectionSucceed(_ connection: Connection, with data: Data) {
        switch connection.name! {
        case "get":
            if let jsonData = try? JSONSerialization.jsonObject(with: data, options: []) as! [Any] {
                print(jsonData)
                
                for element in jsonData {
                    if let obj = element as? [String: Any],
                        let film = Pelicula(json: obj) {
                        self.pelis.append(film)
                    }
                }
                self.tableView.reloadData()
            }
        case "delete":
            if let jsonData = try? JSONSerialization.jsonObject(with: data, options: []) as! [String:Any] {
                
                print(jsonData)
                self.pelis.remove(at: deletedIndexPath.row)
                tableView.deleteRows(at: [deletedIndexPath], with: .fade)
            }
        default:
            print("No connection succeed handler")
        }
        
    }
    
    func connectionFailed(_ connection: Connection, with error: String) {
        
    }
    
    // MARK: - PelisTable Delegate
    
    func saved(peli: Pelicula) {
        self.pelis.append(peli)
        self.tableView.reloadData()
    }
}

