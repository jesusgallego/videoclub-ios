//
//  DetailViewController.swift
//  pelis
//
//  Created by Antonio Pertusa on 14/12/16.
//  Copyright © 2016 Antonio Pertusa. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController, ConnectionDelegate, PelisTableDelegate {


    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var rentButton: UIButton!

    func configureView() {
        // Update the user interface for the detail item.
        if let peli = self.detailItem {
            
            if let rented = peli.rented {
                if rented {
                    self.rentButton?.setTitle("Devolver", for: .normal)
                } else {
                    self.rentButton?.setTitle("Alquilar", for: .normal)
                }
            }

            if let title = peli.title {
                self.navigationItem.title = "\(title))"
                if let year = peli.year {
                    self.navigationItem.title =  "\(title) (\(year))"
                }
            }
            
            self.textView?.text = peli.synopsis
            if let image = peli.image {
                self.imageView?.image = image
            } else {
                self.imageView?.image = UIImage(named:"Placeholder.png")
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.configureView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        DispatchQueue.main.async{ // Para hacer que el textView se visualize desde la primera línea
            self.textView.scrollRangeToVisible(NSMakeRange(0, 0))
        }
    }

    var detailItem: Pelicula? {
        didSet {
            // Update the view.
            self.configureView()
        }
    }
    
    @IBAction func onRentClicked(_ sender: Any) {
        print("Now is \(detailItem?.rented!)")
        let id = detailItem?.id!
        var url = "http://gbrain.dlsi.ua.es/videoclub/api/v1/catalog/\(id!)/rent"
        if (detailItem?.rented!)! {
            url = "http://gbrain.dlsi.ua.es/videoclub/api/v1/catalog/\(id!)/return"
        }
        let session = URLSession(configuration: .default)
        
        let credentials = ("test123", "test123")
        let connection = Connection(name: "edit-rent", delegate: self)
        connection.startConnection(session, with: url, method: "PUT", and: credentials)
    }
    
    @IBAction func onEditClicked(_ sender: Any) {
        
        let pelisEdit = PelisTableViewController()
        pelisEdit.delegate = self
        pelisEdit.peli = Pelicula(detailItem!)
        self.navigationController?.pushViewController(pelisEdit, animated: true)
    }
    
    // MARK: - Connection Delegate
    
    func connectionSucceed(_ connection: Connection, with data: Data) {
        if let jsonData = try? JSONSerialization.jsonObject(with: data, options: []) as! [String:Any] {
            print(jsonData)
            let error = jsonData["error"] as? String
            let hasError = error == "0"
            if hasError == false {
                detailItem?.rented = !(detailItem?.rented!)!
                self.configureView()
            }
        }
    }
    
    func connectionFailed(_ connection: Connection, with error: String) {
        print(error)
    }
    
    // MARK: - Pelis Delegate
    
    func saved(peli: Pelicula) {
        self.detailItem = peli
        let parentController = self.splitViewController!.viewControllers[0] as! UINavigationController
        let masterController = parentController.viewControllers[0] as! MasterViewController
        masterController.tableView.reloadData()
    }
}

